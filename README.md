# MovieDb

MovieDb est une application permettant d'obtenir nombreuses informations sur les films du moment. Nous avons acces à une liste et cela nous dirige vers les informations sur le film que nous avons choisi.


## Information Générale

Nous avons eux de nombreuses difficulutées sur cette application, notamment une toujours d'actualitée. La recuperation de donnée api comme nous l'avons faites ne nous permets pas d'utiliser celle-ci comme nous le voudrons. Apres de longue recherche nous ne sommes arrivé à rien de concluant. L'application est donc toujours en cours de developpement.

Plus tard de nombreuses fonctionnalitées tel que la recherche par nom ou encore la recherche de salle diffusant le film via gps pourront etre ajoutées.

## Technologies et Installations

Pour visualiser l'application vous aurez besoins d'un ordinateur avec les applications visual studio code et android studio. Ainsi qu'installer node.js sur votre ordinateur, cela permettra de lancer votre code js. 
suivez les instructions sur ce lien : https://reactnative.dev/docs/environment-setup

## Equipe

Ce projet à été effectué par Mathis Doré et Thibaud Lamon en collaboration.

