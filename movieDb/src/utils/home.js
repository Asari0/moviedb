import React from 'react';
import {StyleSheet, Text, View, ImageBackground, Pressable, Image} from 'react-native';

export default function Home({ navigation, route }) {

    const onPressHandler = () => {
        navigation.navigate('ListeFilm');
    }
  return (
    <View style={styles.container}>
    <ImageBackground source={require('../images/cine.jpg')} resizeMode="cover" style={styles.image}>
      <View style={styles.bloc_logo}>
        <Image style={styles.logo} source={require('../images/logo4.jpg')}/>
      </View>
      <View style={styles.bloc_bv}>
        <Text style={styles.text_bv}>Bienvenue</Text>
      </View>
      <View style={styles.bloc_button}>
      <Pressable style={styles.button} onPress={onPressHandler}>
        <Text style={styles.text_button}>Liste de films</Text>
      </Pressable>
      </View>
    </ImageBackground>
  </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  image: {
    flex: 1,
    justifyContent: "center",
    position: 'relative',
  },
  text_bv: {
    fontWeight: 'bold',
    fontSize: 50,
    color: 'white',
  },
  text_button: {
    fontSize: 40,
    margin: 10,
  },
  bloc_logo: {
    position: 'absolute',
    top: 20,
 },
  bloc_bv: {
      position: 'absolute',
      top: 214,
      left: 86,
  },
  bloc_button: {
    position: 'absolute',
    margin: 30,
    top: '55%',
    left: '10%',
 },
  button: {
    backgroundColor: 'white',
    borderRadius: 20,
    borderColor: 'black',
    borderWidth: 3,
  },
  logo: {
    height: 200,
    width: 420,
  }
});