import React from 'react';
import { StyleSheet, View, Text, Image, ScrollView, SafeAreaView, FlatList} from 'react-native';
import { withSafeAreaInsets } from 'react-native-safe-area-context';

export default function Details({ route }) {

    return (
        <ScrollView style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.nom_fenetre}>Titre</Text>
            </View>
            <View style={styles.bloc_info}>
                <View style={styles.bloc_affiche}>
                    <Image style={styles.affiche} resizeMode='contain' source={require('../images/le_parrain_test.jpg')}/>
                </View>
                <View style={styles.titre_note}>
                    <View style={styles.date}>
                        <Text style={{fontSize:24, textAlign: 'center'}}>Date</Text>
                    </View>
                    <View style={styles.note}>
                        <Text style={{fontSize:24, textAlign: 'center'}}>Note :{"\n"}10/10</Text>
                    </View>
                </View>                
            </View>
            <View>
                <Text style={{fontSize:24, textAlign: 'left', paddingLeft: 10,}}>Résumé</Text>
            </View>
                <View>
                    <View>
                        <Text style={styles.text}>En 1945, à New York, les Corleone sont une des cinq familles de la mafia. Don Vito Corleone, "parrain" de cette famille, marie sa fille à un bookmaker. Sollozzo, " parrain " de la famille Tattaglia, propose à Don Vito une association dans le trafic de drogue, mais celui-ci refuse. Sonny, un de ses fils, y est quant à lui favorable. Afin de traiter avec Sonny, Sollozzo tente de faire tuer Don Vito, mais celui-ci en réchappe. Michael, le frère cadet de Sonny, recherche alors les commanditaires de l'attentat et tue Sollozzo et le chef de la police, en représailles. Michael part alors en Sicile, où il épouse Apollonia, mais celle-ci est assassinée à sa place. De retour à New York, Michael épouse Kay Adams et se prépare à devenir le successeur de son père...</Text>
                    </View>
                </View> 
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    header:{
        height: 140,
        justifyContent: 'center',
        backgroundColor: 'black',
    },
    container: {
        flex: 1,
    },
    nom_fenetre:{
        fontSize: 35,
        textAlign: 'center',
        color: 'white',
    },
    bloc_info:{
        height: 300,
        display: "flex",
        flexDirection: 'row',
        backgroundColor: 'red',
        justifyContent: 'space-around',
    },
    bloc_affiche:{
        display: 'flex',
        width: '45%',
        backgroundColor: 'black',
        alignItems: 'center',
    },
    affiche:{
        flex: 1,
    },
    titre_note:{
        width: '55%',
        backgroundColor: 'black',
        justifyContent: 'space-around',
    },
    date:{
        display: "flex",
        flexDirection: 'column',
        backgroundColor:'white'
    },
    note:{
        marginTop: "10%",
        backgroundColor:'white'
    },
    nom_film:{
        fontSize: 40,
        textAlign: 'right',
        padding: '5%',
    },
    text: {
        textAlign: 'justify',
        fontSize: 18,
        margin: 10,        
    }
})