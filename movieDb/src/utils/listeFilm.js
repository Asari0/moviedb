import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, StyleSheet, View, Text, Pressable, TouchableWithoutFeedback } from 'react-native';

export default function ListeFilm({ navigation, route })  {

    const onPressHandler = () => {
        navigation.navigate('Details');
    }
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

  async function getMovies(){
     try {
      const response = await fetch('https://api.themoviedb.org/3/discover/movie?api_key=b4b39297702b17c712f5498d5920a3cf');
      const json = await response.json();
      let i = 0
      let array = []
      Object.keys(json.results).map(function(key) {
        Object.keys(json.results[key]).map(function(value){
            console.log(value)
            
            if(value == "title" || value == "vote_average" || value == "backdrop_path" )
            array.push([value, json.results[key][value], i])
            i++
        }); 
        console.log(array)
        // console.log(array)
        i++
        });
      setData(array);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }


 

  useEffect(() => {
    getMovies();
  }, []);

    return (
        <View style={styles.body}>
          <FlatList style={styles.bloc_film}
            data={data}
            keyExtractor={(index) => index[2]}
            renderItem={({ item }) => (
                <Pressable 
                    onPress={onPressHandler}
                    style={({ pressed }) => ({ backgroundColor: pressed ? '#ddd' : 'white' })}
                    >
                    <View style={styles.film}>
                        <Text style={styles.text}>{item[1]}</Text>
                    </View>
                </Pressable>
                
            )}
        />
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 40,
        fontWeight: 'bold',
        margin: 10,
        
    },
    bloc_film: {
        borderColor: 'black',
        borderWidth: 10,
    },
    film: {
      borderColor: 'black',
      borderWidth: 2,
    }
})