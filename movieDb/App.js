import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './src/utils/home';
import ListeFilm from './src/utils/listeFilm';
import Details from './src/utils/details';



const Stack = createNativeStackNavigator();
function App() {

  return (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
      name="Home"
      component={Home}
      />
      <Stack.Screen
      name="ListeFilm"
      component={ListeFilm}
      />
      <Stack.Screen
      name="Details"
      component={Details}
      />
    </Stack.Navigator>
  </NavigationContainer>
)
}

export default App;